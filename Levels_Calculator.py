# Imported modules

import tkinter as tk
from tkinter import ttk
from tkinter.font import BOLD


# Variables

current_width = ["310"]
current_value = ["Yes"]


# Defined commands

def start_levels_calculator():
    first_input = (float(first_entry.get()))
    second_input = float(second_entry.get())
    third_input = float(third_entry.get())
    fourth_input = float(fourth_entry.get())
    cut_percentage_input = float(cut_percentage_entry.get()) / 100

    input_array = [first_input, second_input, third_input, fourth_input]
    input_array.sort()

    input_difference = input_array[3] - input_array[0]
    cut_amount = input_difference * cut_percentage_input

    fgl = input_array[3] - cut_amount
    fgl = "%.3f" % fgl
    fgl2 = float(fgl)
    ffl = fgl2 + (float(current_width[0]) / 1000)
    ffl = "%.3f" % ffl
    ffl2 = float(ffl)
    gffl = ffl2 - 0.075
    gffl = "%.3f" % gffl

    fgl_result_label = tk.Label(tab1, text=str(fgl), font=("", 12, "bold"))
    fgl_result_label.place(relx=0.2, rely=0.7, height=30, relwidth=0.13)

    ffl_result_label = tk.Label(tab1, text=str(ffl), font=("", 12, "bold"))
    ffl_result_label.place(relx=0.5, rely=0.7, height=30, relwidth=0.13)

    gffl_result_label = tk.Label(tab1, text=str(gffl), font=("", 12, "bold"))
    gffl_result_label.place(relx=0.83, rely=0.7, height=30, relwidth=0.13)

def clear_levels_calculator():
    first_entry.delete(0, 99999)
    second_entry.delete(0, 99999)
    third_entry.delete(0, 99999)
    fourth_entry.delete(0, 99999)
    cut_percentage_entry.delete(0, 99999)
    clear_fgl_label = tk.Label(tab1, text="     ", font=("", 12, "bold"))
    clear_fgl_label.place(relx=0.2, rely=0.7, height=30, relwidth=0.13)
    clear_ffl_label = tk.Label(tab1, text="     ", font=("", 12, "bold"))
    clear_ffl_label.place(relx=0.5, rely=0.7, height=30, relwidth=0.13)
    clear_gffl_label = tk.Label(tab1, text="     ", font=("", 12, "bold"))
    clear_gffl_label.place(relx=0.83, rely=0.7, height=30, relwidth=0.13)

def toggle():
    width_switcher = {"310":"385", "385":"310"}
    current_width[0] = width_switcher[current_width[0]]
    toggle_button['text'] = current_width[0]

def start_slope_calculator():
    clear_low_transition_label = tk.Label(tab2, text="     ", font=("", 12, 'bold'))
    clear_low_transition_label.place(relx=0.55, rely=0.81, height=30, relwidth=0.45)
    clear_high_transition_label = tk.Label(tab2, text="     ", font=("", 12, 'bold'))
    clear_high_transition_label.place(relx=0.55, rely=0.66, height=30, relwidth=0.45)
   
    gb_slope_result = ((float(gffl_entry.get()) - float(boundary_rl_entry.get())) / (float(garage_setback_entry.get()) / 1000)) * 100
    br_slope_result = ((float(boundary_rl_entry.get()) - float(road_rl_entry.get())) / (float(road_offset_entry.get()) / 1000)) * 100

    if current_value[0] == "Yes":
        high_transition = float(gffl_entry.get()) - (0.125 * 2)
        low_transition = float(boundary_rl_entry.get()) + (0.125 * 2)
        gb_slope_result = (high_transition - low_transition) / ((float(garage_setback_entry.get()) -4 ) / 1000) * 100
        
        high_transition_label = tk.Label(tab2, text=str(high_transition), font=("", 12, 'bold'))
        high_transition_label.place(relx=0.55, rely=0.66, height=30, relwidth=0.45)

        low_transition_label = tk.Label(tab2, text=str(low_transition), font=("", 12, 'bold'))
        low_transition_label.place(relx=0.55, rely=0.81, height=30, relwidth=0.45)
    
    gb_slope_result = str(round(gb_slope_result, 2)) + "%"
    br_slope_result = str(round(br_slope_result, 2)) + "%"

    gb_slope_result_label = tk.Label(tab2, text=gb_slope_result, font=("", 12, 'bold'))
    gb_slope_result_label.place(relx=0.05, rely=0.66, height=30, relwidth=0.45)
    
    br_slope_result_label = tk.Label(tab2, text=br_slope_result, font=("", 12, 'bold'))
    br_slope_result_label.place(relx=0.05, rely=0.81, height=30, relwidth=0.45)

def clear_slope_calculator():
    gffl_entry.delete(0, 99999)
    boundary_rl_entry.delete(0, 99999)
    road_rl_entry.delete(0, 99999)
    garage_setback_entry.delete(0, 99999)
    road_offset_entry.delete(0, 99999)
    clear_gb_slope_result_label = tk.Label(tab2, text="     ", font=("", 12, 'bold'))
    clear_gb_slope_result_label.place(relx=0.05, rely=0.66, height=30, relwidth=0.45)
    clear_br_slope_result_label = tk.Label(tab2, text="     ", font=("", 12, 'bold'))
    clear_br_slope_result_label.place(relx=0.05, rely=0.81, height=30, relwidth=0.45)
    clear_low_transition_label = tk.Label(tab2, text="     ", font=("", 12, 'bold'))
    clear_low_transition_label.place(relx=0.55, rely=0.81, height=30, relwidth=0.45)
    clear_high_transition_label = tk.Label(tab2, text="     ", font=("", 12, 'bold'))
    clear_high_transition_label.place(relx=0.55, rely=0.66, height=30, relwidth=0.45)


    

def transitions():
    value_switcher = {"Yes":"No", "No":"Yes"}
    current_value[0] = value_switcher[current_value[0]]
    transition_button['text'] = current_value[0]



# Tkinter

root = tk.Tk()


#   Title

root.title("Levels Calculator by DD")


#   Tabs

tab_control = ttk.Notebook(root)

tab1 = ttk.Frame(tab_control, height=600, width=500)
tab2 = ttk.Frame(tab_control, height=600, width=500)

tab_control.add(tab1, text="Levels Calculator")
tab_control.add(tab2, text="Driveway Gradient")
tab_control.pack(expand=1, fill="both")


#   Tab 1


#       Labels

title_label = tk.Label(tab1, text="Level's Calculator", font=("", 24))
title_label.place(relx=0.1, y=20, relheight=0.1, relwidth=0.8)

first_label = tk.Label(tab1, text="1st RL:", font=("", 12))
first_label.place(relx=0.1, rely=0.2, height=30, relwidth=0.3)

second_label = tk.Label(tab1, text="2nd RL:", font=("", 12))
second_label.place(relx=0.1, rely=0.25, height=30, relwidth=0.3)

third_label = tk.Label(tab1, text="3rd RL:", font=("", 12))
third_label.place(relx=0.1, rely=0.3, height=30, relwidth=0.3)

fourth_label = tk.Label(tab1, text="4th RL:", font=("", 12))
fourth_label.place(relx=0.1, rely=0.35, height=30, relwidth=0.3)

slab_height_label = tk.Label(tab1, text="Slab height (mm):", font=("", 12))
slab_height_label.place(relx=0.1, rely=0.45, height=30, relwidth=0.3)

cut_percentage_label = tk.Label(tab1, text="Cut percentage (%):", font=("", 12))
cut_percentage_label.place(relx=0.075, rely=0.55, height=30, relwidth=0.35)

fgl_label = tk.Label(tab1, text="FGL:", font=("", 12))
fgl_label.place(relx=0, rely=0.7, height=30, relwidth=0.3)

ffl_label = tk.Label(tab1, text="FFL:", font=("", 12))
ffl_label.place(relx=0.3, rely=0.7, height=30, relwidth=0.3)

gffl_label = tk.Label(tab1, text="G-FFL:", font=("", 12))
gffl_label.place(relx=0.6, rely=0.7, height=30, relwidth=0.3)


#       Entries

first_entry = tk.Entry(tab1)
first_entry.place(relx=0.5, rely=0.2, height=30, relwidth=0.3)

second_entry = tk.Entry(tab1)
second_entry.place(relx=0.5, rely=0.25, height=30, relwidth=0.3)

third_entry = tk.Entry(tab1)
third_entry.place(relx=0.5, rely=0.3, height=30, relwidth=0.3)

fourth_entry = tk.Entry(tab1)
fourth_entry.place(relx=0.5, rely=0.35, height=30, relwidth=0.3)

cut_percentage_entry = tk.Entry(tab1)
cut_percentage_entry.place(relx=0.5, rely=0.55, height=30, relwidth=0.3)


#       Buttons

toggle_button = tk.Button(tab1, text=current_width[0], font=("", 11, "bold"), command=toggle)
toggle_button.place(relx=0.5, rely=0.45, height=30, relwidth=0.3)

start_button1 = tk.Button(tab1, text="START", command=start_levels_calculator)
start_button1.place(relx=0.15, rely=0.8, height=30, relwidth=0.25)

clear_button1 = tk.Button(tab1, text="CLEAR", command=clear_levels_calculator)
clear_button1.place(relx=0.55, rely=0.8, height=30, relwidth=0.25)


#   Tab 2


#       Labels

driveway_gradient_label = tk.Label(tab2, text="Driveway Gradient", font=("", 24))
driveway_gradient_label.place(relx=0.175, y=20, relheight=0.1, relwidth=0.65)

gffl_label = tk.Label(tab2, text="G-FFL:", font=("", 12))
gffl_label.place(relx=0.1, rely=0.2, height=30, relwidth=0.3)

boundary_rl_label = tk.Label(tab2, text="Boundary RL:", font=("", 12))
boundary_rl_label.place(relx=0.1, rely=0.25, height=30, relwidth=0.3)

road_rl_label = tk.Label(tab2, text="Road RL:", font=("", 12))
road_rl_label.place(relx=0.1, rely=0.3, height=30, relwidth=0.3)

garage_setback_label = tk.Label(tab2, text="Garage setback:", font=("", 12))
garage_setback_label.place(relx=0.1, rely=0.35, height=30, relwidth=0.3)

road_offset_label = tk.Label(tab2, text="Road offset:", font=("", 12))
road_offset_label.place(relx=0.1, rely=0.4, height=30, relwidth=0.3)

gb_slope_label = tk.Label(tab2, text="Garage to boundary slope:", font=("", 12))
gb_slope_label.place(relx=0.05, rely=0.6, height=30, relwidth=0.45)

br_slope_label = tk.Label(tab2, text="Boundry to road slope:", font=("", 12))
br_slope_label.place(relx=0.05, rely=0.75, height=30, relwidth=0.45)

transition_label = tk.Label(tab2, text="Transitions:", font=("", 12))
transition_label.place(relx=0.1, rely=0.5, height=30, relwidth=0.3)

br_slope_label = tk.Label(tab2, text="Garage transition RL:", font=("", 12))
br_slope_label.place(relx=0.55, rely=0.6, height=30, relwidth=0.45)

br_slope_label = tk.Label(tab2, text="Boundary transition RL:", font=("", 12))
br_slope_label.place(relx=0.55, rely=0.75, height=30, relwidth=0.45)


#       Entries

gffl_entry = tk.Entry(tab2)
gffl_entry.place(relx=0.5, rely=0.2, height=30, relwidth=0.3)

boundary_rl_entry = tk.Entry(tab2)
boundary_rl_entry.place(relx=0.5, rely=0.25, height=30, relwidth=0.3)

road_rl_entry = tk.Entry(tab2)
road_rl_entry.place(relx=0.5, rely=0.3, height=30, relwidth=0.3)

garage_setback_entry = tk.Entry(tab2)
garage_setback_entry.place(relx=0.5, rely=0.35, height=30, relwidth=0.3)

road_offset_entry = tk.Entry(tab2)
road_offset_entry.place(relx=0.5, rely=0.4, height=30, relwidth=0.3)


#       Buttons

slope_start_button = tk.Button(tab2, text="START", command=start_slope_calculator)
slope_start_button.place(relx=0.15, rely=0.9, height=30, relwidth=0.25)

slope_start_button = tk.Button(tab2, text="CLEAR", command=clear_slope_calculator)
slope_start_button.place(relx=0.55, rely=0.9, height=30, relwidth=0.25)

transition_button = tk.Button(tab2, text=current_value, font=("", 10, 'bold'), command=transitions)
transition_button.place(relx=0.5, rely=0.5, height=30, relwidth=0.3)


#   Mainloop

root.mainloop()